package taller.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament;
import taller.mundo.teams.SelectionSortTeam;

public class SelectionSortTest extends TestCase{

	private Comparable[] num;
	public void setupEscenario1()
	{
		num = new Comparable[10];
		num[0] = 15;
		num[1] = 2;
		num[2] = 1;
		num[3] = 30;
		num[4] = 57;
		num[5] = 7;
		num[6] = 23;
		num[7] = 48;
		num[8] = 82;
		num[9] = 32;
	}
	public void testTamanho()
	{
		setupEscenario1();
		assertEquals(10, num.length);
	}
	public void testSortAscendente()
	{
		setupEscenario1();
		SelectionSortTeam selection = new SelectionSortTeam();
		num = selection.sort(num, AlgorithmTournament.TipoOrdenamiento.ASCENDENTE);

		assertEquals(10, num.length);
		assertEquals(1, num[0]);

		
		assertEquals(82, num[9]);
		
	}
	public void testSortDescendente()
	{
		setupEscenario1();
		SelectionSortTeam selection = new SelectionSortTeam();
		num = selection.sort(num, AlgorithmTournament.TipoOrdenamiento.DESCENDENTE);

		assertEquals(10, num.length);
		assertEquals(82, num[0]);
		assertEquals(1, num[9]);
		
		
	}
	
}
