package taller.test;

import junit.framework.TestCase;
import taller.mundo.AlgorithmTournament;
import taller.mundo.teams.MergeSortTeam;


public class MergeSortTest extends TestCase{

	private Comparable[] num;
	public void setupEscenario1()
	{
		num = new Comparable[10];
		num[2] = 1;
		num[1] = 2;
		num[5] = 7;
		num[0] = 15;
		num[6] = 23;
		num[3] = 30;
		num[9] = 32;
		num[7] = 48;
		num[4] = 57;
		num[8] = 82;
		
	}
	public void testTamanho()
	{
		setupEscenario1();
		assertEquals(10, num.length);
	}
	public void testSortAscendente()
	{
		setupEscenario1();
		MergeSortTeam merge = new MergeSortTeam();
		num = merge.sort(num, AlgorithmTournament.TipoOrdenamiento.ASCENDENTE);

		assertEquals(10, num.length);
		assertEquals(1, num[0]);
		System.out.println("===============");
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
		
		assertEquals(82, num[9]);
		
	}
	public void testSortDescendente()
	{
		setupEscenario1();
		MergeSortTeam merge = new MergeSortTeam();
		num = merge.sort(num, AlgorithmTournament.TipoOrdenamiento.DESCENDENTE);
		System.out.println("===============");
		for (int i = 0; i < num.length; i++) {
			System.out.println(num[i]);
		}
		assertEquals(10, num.length);
		assertEquals(82, num[0]);
		assertEquals(1, num[9]);
		
		
	}
	
}
