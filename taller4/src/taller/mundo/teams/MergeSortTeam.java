package taller.mundo.teams;

/*
 * MergeSortTeam.java
 * This file is part of AlgorithmRace
 *
 * Copyright (C) 2015 - ISIS1206 Team 
 *
 * AlgorithmRace is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * AlgorithmRace is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with AlgorithmRace. If not, see <http://www.gnu.org/licenses/>.
 */

import java.util.Arrays;
import static taller.mundo.AlgorithmTournament.TipoOrdenamiento;

public class MergeSortTeam extends AlgorithmTeam
{
     public MergeSortTeam()
     {
          super("Merge sort (*)");
          userDefined = true;
     }

     @Override
     public Comparable[] sort(Comparable[] lista, TipoOrdenamiento orden)
     {
          return merge_sort(lista, orden);
     }


     private static Comparable[] merge_sort(Comparable[] lista, TipoOrdenamiento orden)
     {
 
    	 // Trabajo en Clase
    	 int tamanho = lista.length;
    	 if(tamanho == 1)  return lista;
    	 Comparable[] izquierda = new Comparable[tamanho/2];
    	 Comparable[] derecha = new Comparable[tamanho - (tamanho/2)];
    	 for (int i = 0; i < tamanho; i++) {
			if(i >= tamanho/2)
			derecha[i-(tamanho/2)] = lista[i];
			else
			izquierda[i] = lista[i];
		}
    	 izquierda = merge_sort(izquierda, orden);
    	 derecha = merge_sort(derecha,orden);
    	lista = merge(izquierda, derecha, orden);
    	 return lista;
     }
    
     private static Comparable[] merge(Comparable[] izquierda, Comparable[] derecha, TipoOrdenamiento orden)
     {
    	// Trabajo en Clase  
    	 int tamanho = izquierda.length + derecha.length;
    	 int contadorIzquierda = 0;
    	 int contadorDerecha = 0;
    	 Comparable[] lista = new Comparable[tamanho];
    	 if(orden == TipoOrdenamiento.ASCENDENTE)
    	 {
    	 for (int i = 0; i < tamanho; i++) {
			if(contadorIzquierda > izquierda.length-1)
			lista[i] = derecha[contadorDerecha++];
			else if(contadorDerecha > derecha.length-1)
			lista[i] = izquierda[contadorIzquierda++];
			else if(izquierda[contadorIzquierda].compareTo(derecha[contadorDerecha]) < 0)
			lista[i] = izquierda[contadorIzquierda++];
			else
			lista[i] = derecha[contadorDerecha++];
		}
    	 }
    	 else
    	 {
    		 for (int i = 0; i < tamanho; i++) {
    				if(contadorIzquierda > izquierda.length-1)
    				lista[i] = derecha[contadorDerecha++];
    				else if(contadorDerecha > derecha.length-1)
    				lista[i] = izquierda[contadorIzquierda++];
    				else if(izquierda[contadorIzquierda].compareTo(derecha[contadorDerecha]) > 0)
    				lista[i] = izquierda[contadorIzquierda++];
    				else
    				lista[i] = derecha[contadorDerecha++];
    			}
    	 }
    	 
    	 return lista;
     }


}
